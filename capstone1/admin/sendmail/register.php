<?php
// Database connection settings
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "login";

// Create a connection to the database
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Get user input from the registration form
$email = isset($_POST['email']) ? $_POST['email'] : "";
$password = isset($_POST['password']) ? password_hash($_POST['password'], PASSWORD_BCRYPT) : "";

if (empty($email) || empty($password)) {
    echo "Please fill out both email and password fields.";
} else {
    // Insert user data into the database
    $sql = "INSERT INTO users (email, password) VALUES ('$email', '$password')";

    if ($conn->query($sql) === TRUE) {
        echo "Registration successful!";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

// Close the database connection
$conn->close();
?>
