<?php
require 'vendor/autoload.php';

use Twilio\Rest\Client;

$sid = "ACd8d62515b96054ca96813c538e06f6ca";
$token = "a1801c4a53279dc3556dee1441bd2528";
$twilioPhone = "+12524944340";

if (!function_exists("sendSMS")) {
    function sendSMS($message, $to, $from = "")
    {
        global $sid, $token, $twilioPhone;

        if ($from == "") {
            $from = $twilioPhone;
        }

        $client = new Client($sid, $token);

        try {
            $client->messages->create(
                $to,
                [
                    'from' => $from,
                    'body' => $message,
                ]
            );
            echo '<script>alert("Message sent successfully!");</script>';
        } catch (\Exception $e) {
            // JavaScript alert for failure
            echo '<script>alert("Message sending failed!");</script>';
        }
    }
}

//preformatted var_dump function
if (!function_exists('dump')) {
    function dump($var, $adminOnly = false, $localhostOnly = false)
    {
      if ($var !== null) {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
    }

    }
}

//preformatted dump and die function
if (!function_exists('dnd')) {
    function dnd($var)
    {
        dump($var);
        die();
    }
}
?>
