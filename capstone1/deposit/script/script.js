
function openNestedModal() {
    const checkbox = document.getElementById('termsCheckbox');
    if (checkbox.checked) {
      document.getElementById('nestedModal1').style.display = 'block';
    } else {
      alert('Please accept the terms and conditions.');
    }
  }

  function acceptTerms() {
    document.getElementById('nestedModal1').style.display = 'none';
    // Perform any other actions you want when terms are accepted
  }

  function openTermsModal() {
    var modal = document.getElementById("termsModal");
    modal.style.display = "block";
  }

  // Close the terms and conditions modal
  function closeTermsModal() {
    var modal = document.getElementById("termsModal");
    modal.style.display = "none";
    document.getElementById("termsCheckbox").checked = true;
  }

 window.addEventListener('DOMContentLoaded', function() {
  var modalButtons = document.getElementsByClassName('openModalBtn');
  var closeBtns = document.getElementsByClassName('close');
  var nestedModals = document.getElementsByClassName('modal');
  var nestedModalOkButtons = document.getElementsByClassName('ok-button');
  var acceptButtons = document.getElementsByClassName('accept-button');

  for (var i = 0; i < modalButtons.length; i++) {
    modalButtons[i].addEventListener('click', function() {
      var modalId = this.getAttribute('data-modal');
      var modal = document.getElementById(modalId);
      modal.style.display = 'block';
    });
  }

  for (var i = 0; i < closeBtns.length; i++) {
    closeBtns[i].addEventListener('click', function() {
      var modal = this.parentNode.parentNode;
      modal.style.display = 'none';
    });
  }

  for (var i = 0; i < nestedModalOkButtons.length; i++) {
    nestedModalOkButtons[i].addEventListener('click', function() {
      var nestedModal = this.parentNode.parentNode;
      var loginModal = nestedModal.parentNode.parentNode;
      nestedModal.style.display = 'none';
      loginModal.style.display = 'none';
      window.location.href = 'http://localhost/capstone/bookings.php';
    });
  }

  for (var i = 0; i < acceptButtons.length; i++) {
    acceptButtons[i].addEventListener('click', function() {
      var modal = this.parentNode.parentNode;
      modal.style.display = 'none';
    });
  }

  window.addEventListener('click', function(event) {
    for (var i = 0; i < nestedModals.length; i++) {
      if (event.target == nestedModals[i]) {
        nestedModals[i].style.display = 'none';
      }
    }
  });

  var openNestedModalBtns = document.getElementsByClassName('openNestedModalBtn');
  for (var i = 0; i < openNestedModalBtns.length; i++) {
    openNestedModalBtns[i].addEventListener('click', function() {
      var termsCheckbox = this.parentNode.querySelector('#termsCheckbox');
      if (termsCheckbox.checked) {
        var nestedModal = this.parentNode.querySelector('.modal');
        nestedModal.style.display = 'block';
      } else {
        alert('Please accept the terms and conditions.');
      }
    });
  }
});

