<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="deposit/styles.css">
<meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php require('inc/links.php'); ?>
  <title><?php echo $settings_r['site_title'] ?> - Deposit</title>
</head>
<body>

<div style="text-align: center;">
  <h1>GCash Options</h1>
  <button class="openModalBtn" data-modal="loginModal1">Gcash - DD</button>
</div>



<!-- Gcash Section -->
<div id="loginModal1" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <img src="deposit/images/a.png" alt="Gcash" height="200" style="display: block; margin: 0 auto;">
    <form class="unclickable-form">
      <div class="form-group"><br>
      <div style="background-color: #c04141; color: white; padding: 10px; font-weight: bold; border-radius: 5px; text-align: center;">
        <span style="font-size: 18px;">ALERT</span><br>
        <span style="font-size: 14px;">Double check the payment</span>
      </div><br>
        <label for="accountName1">Account Name:</label>
        <input type="text" id="accountName1" name="accountName1" readonly value="ArgaoEcoBay">
      </div>
      <div class="form-group">
        <label for="accountNumber1">Account Number:</label>
        <input type="text" id="accountNumber1" name="accountNumber1" readonly value="123456789">
      </div>
      <input type="checkbox" id="termsCheckbox" required>
      <label for="termsCheckbox">I accept the <a onclick="openTermsModal()" style="color: blue;" style="color: inherit; text-decoration: none;">terms and conditions</a></label>
    </form>
    <button class="openNestedModalBtn" onclick="openNestedModal()">Done</button>

    <div id="nestedModal1" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <h2>Thank you!</h2>
    <p>We would like to express our sincere gratitude for your support and cooperation.</p>
    <button id="nestedModalOkButton1" class="ok-button" onclick="acceptTerms()">OK</button>
  </div>
</div>

  </div>
</div>
<div id="loginModal2" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <img src="deposit/image/2.jpg" alt="Gcash" height="200" style="display: block; margin: 0 auto;">
    <form class="unclickable-form">
      <div class="form-group"><br>
      <div style="background-color: #c04141; color: white; padding: 10px; font-weight: bold; border-radius: 5px; text-align: center;">
        <span style="font-size: 18px;">ALERT</span><br>
        <span style="font-size: 14px;">Double check the payment</span>
    </div><br>
        <label for="accountName2">Account Name:</label>
        <input type="text" id="accountName2" name="accountName2" readonly value="Jane Smith">
      </div>
      <div class="form-group">
        <label for="accountNumber2">Account Number:</label>
        <input type="text" id="accountNumber2" name="accountNumber2" readonly value="987654321">
      </div>
      <input type="checkbox" id="termsCheckbox" required>
      <label for="termsCheckbox">I accept the <a onclick="openTermsModal()" style="color: blue;" style="color: inherit; text-decoration: none;">terms and conditions</a></label>
    </form>
    <button class="openNestedModalBtn" onclick="openNestedModal()">Done</button>

    <div id="nestedModal2" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <h2>Thank you!</h2>
    <p>We would like to express our sincere gratitude for your support and cooperation.</p>
    <button id="nestedModalOkButton2" class="ok-button" onclick="acceptTerms()">OK</button>
  </div>
</div>

  </div>
</div>

<div id="loginModal3" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <img src="deposit/image/3.jpg" alt="Gcash" height="200" style="display: block; margin: 0 auto;">
    <form class="unclickable-form">
      <div class="form-group"><br>
      <div style="background-color: #c04141; color: white; padding: 10px; font-weight: bold; border-radius: 5px; text-align: center;">
        <span style="font-size: 18px;">ALERT</span><br>
        <span style="font-size: 14px;">Double check the payment</span>
    </div><br>
        <label for="accountName3">Account Name:</label>
        <input type="text" id="accountName3" name="accountName3" value="Sarah Johnson" readonly>
      </div>
      <div class="form-group">
        <label for="accountNumber3">Account Number:</label>
        <input type="text" id="accountNumber3" name="accountNumber3" value="555555555" readonly>
      </div>
      <input type="checkbox" id="termsCheckbox" required>
      <label for="termsCheckbox">I accept the <a onclick="openTermsModal()" style="color: blue;" style="color: inherit; text-decoration: none;">terms and conditions</a></label>
    </form>
    <button class="openNestedModalBtn">Done</button>

    <div id="nestedModal3" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <h2>Thank you!</h2>
    <p>We would like to express our sincere gratitude for your support and cooperation.</p>
    <button id="nestedModalOkButton3" class="ok-button" onclick="acceptTerms()">OK</button>
  </div>
</div>

  </div>
</div>




<div id="termsModal" class="modal">
  <div class="modal-content">
    <span class="close">&times;</span>
    <h2>Terms and Conditions</h2>
    <p>
      1. Reservation and Payment:
      <br>
      a. All reservations are subject to availability and must be made in advance.
      <br>
      b. To secure a reservation, a valid credit card or alternative payment method must be provided at the time of booking.
      <br>
      c. The hotel reserves the right to pre-authorize or charge the provided credit card for the full amount or a deposit equivalent to the first night's stay prior to arrival.
      <br>
      d. Payment in full, including any additional charges incurred during the stay, must be settled upon check-out.
      <br>
      e. The hotel accepts major credit cards, cash, and other acceptable payment methods, as indicated by the hotel management.
      <br><br>
      2. Currency and Exchange Rates:
      <br>
      a. All rates, charges, and fees are quoted and payable in the local currency unless otherwise specified.
      <br>
      b. If payment is made using a foreign currency, the hotel's exchange rate or the prevailing rate of the payment processor will apply.
      <br><br>
      3. Additional Charges and Taxes:
      <br>
      a. The guest is responsible for any additional charges incurred during their stay, including but not limited to, room service, minibar consumption, telephone calls, laundry, parking, and any applicable taxes.
      <br>
      b. Taxes imposed by local authorities will be added to the final bill unless exempted by law.
      <br><br>
      4. Cancellation and No-show Policy:
      <br>
      a. Cancellation policies vary depending on the rate plan and booking conditions chosen at the time of reservation. Guests are advised to review the specific cancellation policy associated with their booking.
      <br>
      b. In the event of a no-show, the hotel reserves the right to charge the full amount of the reservation or a penalty as stated in the reservation terms.
      <br><br>
      5. Payment Security:
      <br>
      a. The hotel employs industry-standard security measures to protect the confidentiality and integrity of payment information. However, the hotel shall not be liable for any damages arising from unauthorized access, use, or disclosure of payment information, unless caused by the hotel's gross negligence or willful misconduct.
      <br><br>
      6. Third-Party Payments:
      <br>
      a. Payments made through third-party booking platforms or travel agencies are subject to their terms and conditions. The hotel is not responsible for any issues or disputes arising from such transactions.
      <br><br>
      7. Disputes and Refunds:
      <br>
      a. Any billing disputes or refund requests must be communicated to the hotel's management within 14 days of the transaction date.
      <br>
      b. Refunds, if applicable, will be processed according to the hotel's refund policy and may take a reasonable period to be credited back to the original payment method.
      <br><br>
      8. Modification and Termination:
      <br>
      a. The hotel reserves the right to modify or terminate these payment terms and conditions at any time without prior notice.
      <br>
      b. In the event of termination, any existing reservations will be governed by the terms and conditions in effect at the time of booking.
    </p>
    <button id="termsModalAcceptButton" class="accept-button" onclick="acceptTerms()">Accept</button>
  </div>
</div>


    <script src="deposit/script/script.js"></script>
</body>
</html>
